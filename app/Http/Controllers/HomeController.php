<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        return redirect()->route($request->user()->role);
    }

    public function admin(Request $request){
        if ($request->user()->role != 'admin'){
            $request->session()->flash('status','You must be admin to access.');
            return redirect()->route($request->user()->role);
        }else{
            return view('home');
        }
    }

    public function customer(Request $request){
        if ($request->user()->role != 'customer'){
            $request->session()->flash('status','You are not customer');
            return redirect()->route($request->user()->role);
        }else{
            return view('home');
        }
    }
}
