<?php

use Illuminate\Database\Seeder;
use App\user;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = array(
            array(
                'name' => 'Admin',
                'email' => 'admin@roomies.com',
                'password' => Hash::make('admin123'),
                'status' => 'verified',
                'role' => 'admin'
            ),
            array(
                'name' => 'Customer',
                'email' => 'customer@roomies.com',
                'password' => Hash::make('customer123'),
                'status' => 'verified',
                'role' => 'customer'
            )
        );

        foreach ($users as $data){
            $user_data = User::where('email', $data['email'])->first();
            if(!$user_data){
                $user_data = new User();
                $user_data->fill($data);
                $user_data->save();
            }
        }
    }
}
