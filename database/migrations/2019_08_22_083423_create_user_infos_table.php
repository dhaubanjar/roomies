<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserInfosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_infos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('CASCADE')->onUpdate('CASCADE');
            $table->string('image',60)->nullable();
            $table->enum('looking_for',['just me','couple','friends']);
            $table->string('age');
            $table->enum('gender',['male','female','other']);
            $table->string('budget',10);
            $table->string('occupation',50);
            $table->date('move_date');
            $table->string('stay_length');
            $table->boolean('smoker')->default(false);
            $table->boolean('cat')->default(false);
            $table->boolean('dog')->default(false);
            $table->boolean('student')->default(false);
            $table->boolean('lgbt')->default(false);
            $table->boolean('cannabis')->default(false);
            $table->boolean('teamups')->default(false);
            $table->text('description');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_infos');
    }
}
